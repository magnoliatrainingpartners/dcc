package com.challenge.disney.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.disney.main.bl.IVoteChartService;
import com.challenge.disney.main.domain.VoteChart;

@RestController
@RequestMapping("/v1/votes")
public class VoteChartController {
	
	@Autowired
	private IVoteChartService voteChartService;
	
	@RequestMapping(method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> votePicture(@RequestBody VoteChart vote){
		return new ResponseEntity<>(this.voteChartService.votePicture(vote), HttpStatus.OK);
	}

}