package com.challenge.disney.main.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DT_VOTE_CHART")
public class VoteChart {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="USER_UUID")
	private String userUUID;
	
	@Column(name="PICTURE_UUID")
	private String pictureUUID;
	
	@Column(name="VOTE")
	private String vote;

	public VoteChart() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(String userUUID) {
		this.userUUID = userUUID;
	}

	public String getPictureUUID() {
		return pictureUUID;
	}

	public void setPictureUUID(String pictureUUID) {
		this.pictureUUID = pictureUUID;
	}

	public String getVote() {
		return vote;
	}

	public void setVote(String vote) {
		this.vote = vote;
	}

}