package com.challenge.disney.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.disney.main.domain.VoteChart;

public interface IVoteChartDAO extends JpaRepository<VoteChart, Long> {

	VoteChart getVoteChartByUserUUIDAndPictureUUID(String userUUID, String pictureUUID);

}