package com.challenge.disney.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.disney.main.domain.AppUser;

public interface IAppUserDAO extends JpaRepository<AppUser, Long> {
	
	public AppUser getAppUserByIdentifierCode(String identifierCode);

}
