package com.challenge.disney.main.bl;

import com.challenge.disney.main.domain.VoteChart;
import com.challenge.disney.main.request.ResponseCodeRequest;

public interface IVoteChartService {
	
	ResponseCodeRequest votePicture(VoteChart vote);

}
