package com.challenge.disney.main.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DT_DOG_GALLERY")
public class DogPicture {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PICTURE_URL")
	private String pictureUrl;

	@Column(name = "BREED")
	private String breed;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "IDENTIFIER_CODE")
	private String identifierCode;

	@Column(name = "UP_VOTES")
	private Long upVotes = 0l;

	@Column(name = "DOWN_VOTES")
	private Long downVotes = 0l;

	public Long getUpVotes() {
		return upVotes;
	}

	public void setUpVotes(Long upVotes) {
		this.upVotes = upVotes;
	}

	public Long getDownVotes() {
		return downVotes;
	}

	public void setDownVotes(Long downVotes) {
		this.downVotes = downVotes;
	}

	public DogPicture() {
		super();
	}

	public DogPicture(String pictureUrl, String breed, String description) {
		super();
		this.pictureUrl = pictureUrl;
		this.breed = breed;
		this.description = description;
		this.identifierCode = UUID.randomUUID().toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdentifierCode() {
		return identifierCode;
	}

	public void setIdentifierCode(String identifierCode) {
		this.identifierCode = identifierCode;
	}

}