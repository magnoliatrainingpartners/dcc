package com.challenge.disney.main.bl.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.disney.main.bl.IDogPictureService;
import com.challenge.disney.main.bl.IVoteChartService;
import com.challenge.disney.main.domain.VoteChart;
import com.challenge.disney.main.repository.IVoteChartDAO;
import com.challenge.disney.main.request.ResponseCodeRequest;

@Service
public class VoteChartServiceImpl implements IVoteChartService {

	@Autowired
	private IVoteChartDAO voteChartDAO;

	@Autowired
	private IDogPictureService dogPictureService;

	@Override
	public ResponseCodeRequest votePicture(VoteChart vote) {
		VoteChart element = this.voteChartDAO.getVoteChartByUserUUIDAndPictureUUID(vote.getUserUUID(), vote.getPictureUUID());
		if (element != null && element.getVote().toUpperCase().equals(vote.getVote().toUpperCase())) {
			return new ResponseCodeRequest("ERROR", "The user has already vote this picture");
		} else if (element != null && !element.getVote().toUpperCase().equals(vote.getVote().toUpperCase())) {
//			ResponseCodeRequest response = this.dogPictureService.updateVoteChart(vote.getVote());
			return null;
		} else {
			this.voteChartDAO.save(vote);
			return new ResponseCodeRequest("SUCCESS", "Your vote has been saved");
		}
	}

}