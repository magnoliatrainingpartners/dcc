package com.challenge.disney.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.disney.main.bl.IAppUserService;
import com.challenge.disney.main.domain.AppUser;

@RestController
@RequestMapping("/v1/users")
public class AppUserController {

	@Autowired
	private IAppUserService appUserService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllUsers() {
		return new ResponseEntity<>(this.appUserService.listAllUsers(), HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addNewUser(@RequestBody AppUser user) {
		return new ResponseEntity<>(appUserService.createNewUser(user), HttpStatus.BAD_REQUEST);
	}

}