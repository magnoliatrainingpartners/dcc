package com.challenge.disney.main.bl.impl;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.disney.main.bl.IAppUserService;
import com.challenge.disney.main.domain.AppUser;
import com.challenge.disney.main.repository.IAppUserDAO;
import com.challenge.disney.main.request.ResponseCodeRequest;

@Service
public class AppUserServiceImpl implements IAppUserService {

	@Autowired
	private IAppUserDAO appUserDAO;

	@Override
	public List<AppUser> listAllUsers() {
		List<AppUser> users = this.appUserDAO.findAll();
		Collections.sort(users, (usr1, usr2) -> usr2.getSurname().compareTo(usr1.getSurname()));
		return users;
	}

	@Override
	public ResponseCodeRequest createNewUser(AppUser user) {
		user.setIdentifierCode(UUID.randomUUID().toString());
		this.appUserDAO.save(user);
		return new ResponseCodeRequest("SUCCESS", user.getIdentifierCode());
	}

}