package com.challenge.disney.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.disney.main.bl.IDogPictureService;
import com.challenge.disney.main.domain.DogPicture;

@RestController
@RequestMapping("/v1/pictures")
public class DogGalleryController {

	@Autowired
	IDogPictureService dogPictureService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllPictures() {
		return new ResponseEntity<>(this.dogPictureService.getAllGalleries(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{breedName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getGalleryByBreed(@PathVariable("breedName") String breedName) {
		return new ResponseEntity<>(this.dogPictureService.getGalleryByBreed(breedName), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addPicture(@RequestBody DogPicture dogPicture) {
		return new ResponseEntity<>(this.dogPictureService.savePicture(dogPicture), HttpStatus.OK);
	}

}