package com.challenge.disney.main.bl.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.disney.main.bl.IDogPictureService;
import com.challenge.disney.main.domain.DogPicture;
import com.challenge.disney.main.repository.IDogPictureDAO;
import com.challenge.disney.main.request.ResponseCodeRequest;

@Service
public class DogPictureServiceImpl implements IDogPictureService {

	public static final int ZERO_UPDATE = 0;
	public static final int ONE_ROW_UPDATE = 1;

	@Autowired
	private IDogPictureDAO dogPictureDao;

	@Override
	public Map<String, List<DogPicture>> getAllGalleries() {
		List<DogPicture> gallery = this.dogPictureDao.findAll();

		Map<String, List<DogPicture>> galleryByBreed = gallery.stream().collect(Collectors.groupingBy(pic -> pic.getBreed()));

		Set<String> breeds = galleryByBreed.keySet();

		for (String breedName : breeds) {
			Collections.sort(galleryByBreed.get(breedName), (pic1, pic2) -> pic1.getUpVotes().compareTo(pic2.getUpVotes()));
		}

		return galleryByBreed;
	}

	@Override
	public List<DogPicture> getGalleryByBreed(String breedName) {
		List<DogPicture> gallery = this.dogPictureDao.getAllDogsByBreed(breedName);
		Collections.sort(gallery, (pic1, pic2) -> pic1.getUpVotes().compareTo(pic2.getUpVotes()));
		return gallery;
	}

	@Override
	public ResponseCodeRequest savePicture(DogPicture dogPicture) {
		dogPicture.setIdentifierCode(UUID.randomUUID().toString());
		this.dogPictureDao.save(dogPicture);
		return new ResponseCodeRequest("SUCCESS", dogPicture.getIdentifierCode());
	}

}