package com.challenge.disney.main.bl;

import java.util.List;
import java.util.Map;

import com.challenge.disney.main.domain.DogPicture;
import com.challenge.disney.main.request.ResponseCodeRequest;

public interface IDogPictureService {
	
	Map<String, List<DogPicture>> getAllGalleries();
	
	List<DogPicture> getGalleryByBreed(String name);

	ResponseCodeRequest savePicture(DogPicture dogPicture);
	
}